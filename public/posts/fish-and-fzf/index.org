#+BEGIN_COMMENT
.. title: Fish 와 fzf
.. slug: fish-and-fzf
.. date: 2019-08-11 12:31:10 UTC+09:00
.. tags: fish,fzf
.. category: 개발환경
.. link: 
.. description: 
.. type: text

#+END_COMMENT

문득 ~echo $PATH~ 를 해보았는데 이런 저런 안쓰는 디렉토리들이 있어서 찾아다니며 지웠다.
#+BEGIN_SRC sh
echo $PATH
/Users/apple/.fzf/bin /Users/apple/bin /usr/local/bin /usr/bin /bin /usr/sbin /sbin
#+END_SRC

근데 저 ~.fzf/bin~ 은 여기저기를 뒤져봐도 적혀 있는 곳이 없었다. 내가 살펴본 곳들은
@@html:<!-- TEASER_END -->@@

 - ~/.bash_profile
 - ~/.bashrc
 - ~/.config/fish/config.fish
 - /etc/paths
 - /etc/paths.d

결과적으로 fish는 환경 변수를 .config/fish 디렉토리에 독자적으로 저장하고 있었다. 다음과 같이 지울 수 있다.
#+BEGIN_SRC sh
set -e fish_user_paths
#+END_SRC
