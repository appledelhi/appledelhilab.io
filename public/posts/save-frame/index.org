#+BEGIN_COMMENT
.. title: Emacs 시작할 때 이전 frame 복원
.. slug: save-frame
.. date: 2019-08-05 00:04:45 UTC+09:00
.. tags: emacs,frame
.. category: emacs
.. link: 
.. description: 
.. type: text

#+END_COMMENT

Emacs는 종료할 때 프레임의 위치와 크기를 저장하지 않는다. desktop-save-mode를 사용하면
되지만 이 모드는 frame 위치와 크기 뿐만 아니라, 버퍼 등등 온갖 상태를 저장하므로
emacs를 재시작 하기가 부담스럽다.
@@html:<!-- TEASER_END -->@@

그래서 간단하게 frame의 위치와 크기를 저장하고 복원하는 함수를 만들었다.
#+BEGIN_SRC emacs-lisp
;;; Save frame

(defun my-save-frame ()
  "Gets the current frame's geometry and saves to ~/.emacs.d/frame.el."
  (let ((frameg-left (frame-parameter (selected-frame) 'left))
        (frameg-top (frame-parameter (selected-frame) 'top))
        (frameg-width (frame-text-width))
        (frameg-height (frame-text-height))
        (frameg-file "~/.emacs.d/frame.el"))
    (with-temp-buffer
      (insert
       ";;; This file stores the previous emacs frame's geometry.\n"
       "(setq initial-frame-alist\n"
       (format "'((top . %d)\n" (max frameg-top 0))
       (format "  (left . %d)\n" (max frameg-left 0))
       (format "  (width . (text-pixels . %d))\n" (max frameg-width 0))
       (format "  (height . (text-pixels . %d))))\n" (max frameg-height 0)))
      (when (file-writable-p frameg-file)
        (write-file frameg-file)))))

;;; Load frame

(defun my-load-frame ()
  "Loads ~/.emacs.d/frame.el which should load the previous frame's geometry."
  (let ((frameg-file "~/.emacs.d/frame.el"))
    (when (file-readable-p frameg-file)
      (load-file frameg-file))))

(my-load-frame)

(when (display-graphic-p)
  (add-hook 'kill-emacs-hook #'my-save-frame))
#+END_SRC
